class UsersController < ApplicationController
  def show
    @user = User.find(params[:id])
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)

    if @user.save
      render '_sign_up_success'
    else
      render 'new'
    end
  end

  def sign_up
    @user = User.new
    render 'new'
  end

  def log_in
    @user = User.find_by(user_params)

    if @user
      redirect_to @user
    else
      render '_sign_in_error'
    end
  end

  private
    def user_params
      params.require(:user).permit(:username, :password)
    end
end
