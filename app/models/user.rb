class User < ActiveRecord::Base
  # Validations
  validates :username, presence: true, length: { minimum: 5, maximum: 16 }
  validates :password, presence: true, length: { minimum: 5, maximum: 32 }
end
